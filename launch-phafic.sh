#!/bin/bash

# USE: launch-phafic.sh <timeout> <num_mpi_ranks> <mpi_ranks_per_node> <hostfile> <param1> <param2> <...>
# EXAMPLE: launch-phafic.sh 10m 512 16 hosts.txt 0.5 2.2

# OUTPUT: "0 value" OR "error_code NaN"

# ERROR CODES:
# -1:  incorrect input arguments
# -2:  I_MPI_ROOT not defined
# -3:  mpiexec not available
# -4:  hostfile not available
# -5:  singularity not available
# -6:  container not available
# -7:  unable to create output directory
# -8:  bc or timeout tool missing
# -9:  morphSimilarity.py not executable
# -10: query image not available
# -11: similarity calculation failed
# -101: simulation failed (no output)
# -102: simulation not converged (output incomplete)

# QUERY MORPHOLOGY
QUERY_IMAGE="examples/query/400x100_01.jpg"

# CONTAINER AND PROBLEM CONFIGURATION
MORPH_SIMILARITY="morphSimilarity/morphSimilarity.py"

CONTAINER="./phaficv01.simg"
CONTAINER_BINDS="/projects:/projects"

APP="binary_CH"
APP_ARGS="-snes_atol 1e-10"

LX=4.0
NELEMX=399
LY=1.0
NELEMY=99


# ----- NO NEED TO EDIT BELOW! -----

TIMEOUT="$1"
MPI_N="$2"
MPI_PPN="$3"
HOSTFILE="$4"
shift 4

if [ -z "$1" ]; then
  echo "-1 NaN"
  exit -1
fi

QUERY_IMAGE=`readlink -f $QUERY_IMAGE`
MORPH_SIMILARITY=`readlink -f $MORPH_SIMILARITY`
CONTAINER=`readlink -f $CONTAINER`

create_phafic_config() {
  APP="$1"
  shift
  if [ "$APP" == "binary_CH" ]; then
    echo "
Lx = $LX
Nelemx = $NELEMX
Ly = $LY
Nelemy = $NELEMY
meanPhi = $1
chi12 = $2
varPhi = 0.001
dt = 1e-4
Cn = 0.01
ifRandSeed = 1
D = 1
timeScheme = 3
typeOfAdapt = 1
ifDconst = 1
ifEvapor = 0
typeOfFreeEnergy = 1
m1 = 1
m2 = 1
totalT = 0.5
typeOfIC = 1
adptTol = 1e-3
nsd = 2
ifBoxGrid = 1
orderOfBF = 1
ifDD = 1
ifpBC = 1
varListOutput = [phi]
outputingStrategy = 2
outputEveryN = 20
"
  elif [ "$APP" == "ternary_CH" ]; then
    echo "
Lx = $LX
Nelemx = $NELEMX
Ly = $LY
Nelemy = $NELEMY
ke = 0.0073
meanPhi1 = 0.086
meanPhi2 = 0.114
dt = 1e-4
Cn = 0.001
ifRandSeed = 1
varPhi1 = 0.01
varPhi2 = 0.01
D1 = 0.001
D2 = 0.005
timeScheme = 1
typeOfAdapt = 1
ifDconst = 0
ifEvapor = 1
typeOfFreeEnergy = 2
m1 = 89
m2 = 7
m3 = 1
chi12 = 1.0
chi13 = 0.4
chi23 = 0.9
beta = 0.0001
h11 = 0.0
h12 = 0.0
h21 = 0.0
h22 = 0.0
hD = 0.001
totalT = 20000000
typeOfIC = 2
adptTol = 1e-3
nsd = 2
ifBoxGrid = 1
hCurr = 1.00
orderOfBF = 1
ifDD = 1
ifpBC = 2
varListOutput = [phi1, phi2, hScaled, lambda]
outputingStrategy = 0
"
  fi
}


# check MPI
if [ -z "$I_MPI_ROOT" ]; then
  echo "-2 NaN"
  exit -1
fi

if [ ! -x `which mpiexec` ]; then
  echo "-3 NaN"
  exit -1
fi

if [ ! -f $HOSTFILE ]; then
  echo "-4 NaN"
  exit -1
fi

# check singularity
if [ ! -x `which singularity` ]; then
  echo "-5 NaN"
  exit -1
fi

if [ ! -f "$CONTAINER" ]; then
  echo "-6 NaN"
  exit -1
fi

# check tools
if [ ! -x `which bc` ]; then
  echo "-8 NaN"
  exit -1
fi

if [ ! -x `which timeout` ]; then
  echo "-8 NaN"
  exit -1
fi

if [ ! -x "$MORPH_SIMILARITY" ]; then
  echo "-9 NaN"
  exit -1
fi

if [ ! -f "$QUERY_IMAGE" ]; then
  echo "-10 NaN"
  exit -1
fi

# prepare container for execution
if [ -n "$CONTAINER_BINDS" ]; then
  CONTAINER_BINDS="$I_MPI_ROOT:/intel-mpi,$CONTAINER_BINDS"
else
  CONTAINER_BINDS="$I_MPI_ROOT:/intel-mpi"
fi

SINGULARITY_CALL="singularity run --bind $CONTAINER_BINDS --app $APP $CONTAINER"

# prepare for execution
TIME_STAMP=`date +"%s"`
OUT_DIR="$APP-$TIME_STAMP"

mkdir -p $OUT_DIR

if [ ! -d "$OUT_DIR" ]; then
  echo "-7 NaN"
  exit -1
fi

mv $HOSTFILE $OUT_DIR
cd $OUT_DIR

HOSTFILE=`basename $HOSTFILE`

create_phafic_config $APP $@ > config.txt

# we are ready to execute
timeout $TIMEOUT mpiexec -n $MPI_N -ppn $MPI_PPN -f $HOSTFILE $SINGULARITY_CALL $APP_ARGS > results.stdout 2> results.stderr

# get results into output folder
mkdir results
mv -f dataCH.*.plt results/ 2> /dev/null

if [ ! "$(ls -A results/)" ]; then
  echo "-101 NaN"
  exit -1
fi

if [ ! -f "dataCH_final.plt" ]; then
  echo "-102 NaN"
  exit -1
fi

# compute objective
RES=`$MORPH_SIMILARITY $NELEMX $NELEMY results/ $QUERY_IMAGE` 2> similarity.stderr

if [ -s "similarity.stderr" ]; then
  echo "-11 NaN"
else
  echo "0 $RES"
fi
