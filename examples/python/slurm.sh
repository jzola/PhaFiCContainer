#!/bin/bash

####### PLANEX SPECIFIC - DO NOT EDIT THIS SECTION
#SBATCH --clusters=faculty
#SBATCH --partition=planex
#SBATCH --qos=planex
#SBATCH --account=jzola
#SBATCH --exclusive
#SBATCH --mem=64000

####### CUSTOMIZE THIS SECTION FOR YOUR JOB
#SBATCH --job-name="BO Test"
#SBATCH --nodes=8
#SBATCH --ntasks-per-node=20
#SBATCH --output=%j.stdout
#SBATCH --error=%j.stderr
#SBATCH --time=72:00:00

# IntelMPI is required by the simulation container
module load intel-mpi/2020.2
module load python/py38-anaconda-2020.11

# get list of ALL available nodes
HOSTS="hosts.$SLURM_JOBID"
scontrol show hostname $SLURM_JOB_NODELIST > $HOSTS

# run
./demo.py 10m $SLURM_NTASKS $SLURM_NTASKS_PER_NODE $HOSTS
