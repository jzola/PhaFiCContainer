#!/usr/bin/env python3

import subprocess
import sys

# PHI range = [0.5, 0.65]
PHI="0.55"

# CHI range = [2.2, 4.0]
CHI="2.25"

cmd = ["./launch-phafic.sh", sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], PHI, CHI]
res = subprocess.run(cmd, capture_output=True)

print("OK:", res)
