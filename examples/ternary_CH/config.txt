ke = 0.0073
meanPhi1 = 0.086
meanPhi2 = 0.114
dt = 1e-4

Cn = 0.001

ifRandSeed = 1
varPhi1 = 0.01
varPhi2 = 0.01

D1 = 0.001
D2 = 0.005

timeScheme = 1
typeOfAdapt = 1

ifDconst = 0
ifEvapor = 1
typeOfFreeEnergy = 2
m1 = 89
m2 = 7
m3 = 1
chi12 = 1.0
chi13 = 0.4
chi23 = 0.9
beta = 0.0001

h11 = 0.0
h12 = 0.0
h21 = 0.0
h22 = 0.0
hD = 0.001

totalT = 20000000

typeOfIC = 2
adptTol = 1e-3

nsd = 2
ifBoxGrid = 1
Lx = 0.5
Ly =1.0
Nelemx = 250
Nelemy = 500
hCurr = 1.00

orderOfBF = 1
ifDD = 1
ifpBC = 2

varListOutput = [phi1, phi2, hScaled, lambda]
outputingStrategy = 0
